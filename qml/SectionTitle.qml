import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItems

Rectangle {
    id: root

    width: parent ? parent.width : undefined
    height: childrenRect.height
    property alias title: label.text
    color: theme.palette.normal.background

    Column {
        anchors {
            left: parent.left
            right: parent.right
        }

        Label {
            id: label
            height: units.gu(4)
            fontSize: "medium"
            verticalAlignment: Text.AlignVCenter
        }

        ListItems.ThinDivider {}
    }
}
