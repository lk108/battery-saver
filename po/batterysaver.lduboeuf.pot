# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the batterysaver.lduboeuf package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: batterysaver.lduboeuf\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-07 15:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:106
msgid "Battery Saver"
msgstr ""

#: ../qml/Main.qml:107
msgid ""
"This app allows to switch to preferred network when cellular data is on, and "
"toggle to default when cellular data is off, app can be closed if running "
"state is shown"
msgstr ""

#: ../qml/Main.qml:120 ../qml/Main.qml:146 batterysaver.desktop.in.h:1
msgid "BatterySaver"
msgstr ""

#: ../qml/Main.qml:135
msgid "No SIM card detected!"
msgstr ""

#: ../qml/Main.qml:147
msgid "Checking..."
msgstr ""

#: ../qml/Main.qml:147
msgid "Running..."
msgstr ""

#: ../qml/Main.qml:147
msgid "Stopped"
msgstr ""

#: ../qml/Main.qml:192
msgid "When cellular data is off:"
msgstr ""

#: ../qml/Main.qml:197 ../qml/Main.qml:212
msgid "preferred network:"
msgstr ""

#: ../qml/Main.qml:207
msgid "When cellular data is on:"
msgstr ""

#: ../qml/Main.qml:223
msgid "only select when screen is unlocked"
msgstr ""

#: ../qml/Main.qml:241
msgid "toggle off wifi"
msgstr ""

#: ../qml/Main.qml:255
msgid "When wifi is on:"
msgstr ""

#: ../qml/Main.qml:261
msgid "toggle off cellular data"
msgstr ""
