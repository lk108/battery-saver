package main

import (
	"fmt"
	"github.com/godbus/dbus/v5"
	"os"
)

func main() {
	fmt.Println("hello world")

	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to session bus:", err)
		os.Exit(1)
	}
	defer conn.Close()

	if err = conn.AddMatchSignal(
		dbus.WithMatchObjectPath("/com/lomiri/connectivity1/Private"),
		dbus.WithMatchInterface("org.freedesktop.DBus.Properties"),
		dbus.WithMatchSender("com.lomiri.connectivity1"),
		dbus.WithMatchMember("PropertiesChanged"),
	); err != nil {
		panic(err)
	}

	c := make(chan *dbus.Signal, 10)
	conn.Signal(c)
	for v := range c {
		fmt.Println(v)
		//switch v.Name {
		//case "org.freedesktop.DBus.Properties.PropertiesChanged":
		props := v.Body[1].(map[string]dbus.Variant)
		if v, ok := props["MobileDataEnabled"]; ok {
			dataEnabled := v.Value().(bool)
			fmt.Printf("dataEnabled: %b\n", dataEnabled)
		}

		//}
	}
}
