#include "batterysaver.h"

#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusArgument>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusMetaType>
#include <QDebug>
#include <QTimer>
#include <QProcess>

#define SET_PREF_COMMAND "/usr/share/ofono/scripts/set-tech-preference %1 %2"
#define DBUS_INDICATOR_NETWORK_SERVICE "com.lomiri.indicator.network"
#define DBUS_INDICATOR_NETWORK_PATH "/com/lomiri/indicator/network"
#define DBUS_INDICATOR_NETWORK_INTERFACE "org.gtk.Actions"

BatterySaver::BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, bool _disableCellularDataWifiOn, bool _mobileSpeedDependsOnLockState, QObject *parent)
    : defaultTechPrefCommand(), prefTechPrefCommand(), disableWifi(false), QObject(parent)
{

    qDebug() << "BatterySaver(): batterysaver starting";
    qDebug() << "BatterySaver():" << "\n\tsimSlot" << simSlot << "\n\tdefaultNetwork" << defaultNetwork << "\n\tpreferredNetwork" << prefferedNetwork << "\n\tdisableWifi" << _disableWifi << "\n\tdisableCellular" << _disableCellularDataWifiOn << "\n\tmobileSpeedDependsOnLockState" << _mobileSpeedDependsOnLockState;

    defaultTech = defaultNetwork;
    preferredTech = prefferedNetwork;

    defaultTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, defaultNetwork);
    prefTechPrefCommand = QString(SET_PREF_COMMAND).arg(simSlot, prefferedNetwork);
    disableWifi = _disableWifi;
    disableCellularDataWifiOn = _disableCellularDataWifiOn;
    mobileSpeedDependsOnLockState = _mobileSpeedDependsOnLockState;
    
    screenIsLocked = isScreenLocked();
    mobileDataActive = interfaceEnabled("mobiledata.enabled");
    hotspotActive = interfaceEnabled("hotspot.enable");

    qDebug() << "BatterySaver(): screen locked: " << screenIsLocked;
    qDebug() << "BatterySaver(): mobile data enabled: " << mobileDataActive;
    qDebug() << "BatterySaver(): hotspot enabled: " << hotspotActive;

    qDebug() << "BatterySaver(): defaultCmd" << defaultTechPrefCommand;
    qDebug() << "BatterySaver(): prefCmd" << prefTechPrefCommand;


    bool result = QDBusConnection::sessionBus().connect(
        "com.lomiri.connectivity1",  // service
        "/com/lomiri/connectivity1/Private", // path
        "org.freedesktop.DBus.Properties",  // interface
        QStringLiteral("PropertiesChanged"), // signal
        this,
        SLOT(propertiesChanged(QString,QVariantMap,QStringList)));

    if (!result) {
        qWarning() << "BatterySaver(): error connecting dbus session";
    }

    result = QDBusConnection::sessionBus().connect(
        "com.lomiri.connectivity1",  // service
        "/com/lomiri/connectivity1/NetworkingStatus", // path
        "org.freedesktop.DBus.Properties",  // interface
        QStringLiteral("PropertiesChanged"), // signal
        this,
        SLOT(onNetworkingStatusChanged(QString,QVariantMap,QStringList)));

    if (!result) {
        qWarning() << "BatterySaver(): error connecting dbus NetworkingStatus";
    }

    if (mobileSpeedDependsOnLockState) {

        result = QDBusConnection::sessionBus().connect(
            "com.lomiri.LomiriGreeter",  // service
            "/com/lomiri/LomiriGreeter", // path
            "org.freedesktop.DBus.Properties",  // interface
            QStringLiteral("PropertiesChanged"), // signal
            this,
            SLOT(lockStateChanged(QString,QVariantMap,QStringList)));

        if (!result) {
            qWarning() << "BatterySaver(): error connecting dbus LomiriGreeter";
        }
    }
}

void BatterySaver::propertiesChanged(const QString &interface,
                                     const QVariantMap &changed,
                                     const QStringList &invalidated)

{
    qDebug() << "propertiesChanged(): changed:" << changed;
    mobileDataActive = QVariant(changed["MobileDataEnabled"]).toBool();
    if (mobileDataActive) {
        qDebug() << "propertiesChanged(): ok mobileDataActive true";
        if (!mobileSpeedDependsOnLockState || !screenIsLocked || hotspotActive) {
            qDebug() << "propertiesChanged(): NOW SWITCHING TO" << preferredTech;
            auto& cmd = prefTechPrefCommand;
            QTimer::singleShot(1000, this, [cmd, this] () {
                QProcess process;
                process.start(cmd);
                process.waitForFinished(1000);
                if (disableWifi &&  interfaceEnabled("wifi.enable")) {
                    qDebug() << "propertiesChanged(): disabling wifi";
                    deactivateInterface("wifi.enable");
                }
            });
        } else {
            qDebug() << "propertiesChanged(): screen locked, not switching to preferred tech";
            if (disableWifi &&  interfaceEnabled("wifi.enable")) {
                qDebug() << "propertiesChanged(): disabling wifi";
                deactivateInterface("wifi.enable");
            }
        }
    } else {
        qDebug() << "propertiesChanged(): ok mobileDataActive false";
        qDebug() << "propertiesChanged(): NOW SWITCHING TO" << defaultTech;
        auto& cmd = defaultTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
        });
    }
}

void BatterySaver::onNetworkingStatusChanged(const QString &interface, const QVariantMap &changed, const QStringList &invalidated)
{
    qDebug() << "onNetworkingStatusChanged(): status changed" << changed;

    mobileDataActive = interfaceEnabled("mobiledata.enabled");
    qDebug() << "onNetworkingStatusChanged(): mobileDataActive" << mobileDataActive;

    hotspotActive = interfaceEnabled("hotspot.enable");
    qDebug() << "onNetworkingStatusChanged(): hotspotActive" << hotspotActive;

    if (disableCellularDataWifiOn) {
        bool wifiEnabled = QVariant(changed["WifiEnabled"]).toBool();
        qDebug() << "onNetworkingStatusChanged(): wifiEnabled" << wifiEnabled;
        if (!wifiEnabled) {
            return;
        }

        if (hotspotActive) {
            return;
        }

        if (mobileDataActive) {
            qDebug() << "onNetworkingStatusChanged(): deactivating mobile data";
            deactivateInterface("mobiledata.enabled");
        }
    }
}

void BatterySaver::lockStateChanged(const QString &interface, const QVariantMap &changed, const QStringList &invalidated)
{
    screenIsLocked = QVariant(changed["IsActive"]).toBool();

    qDebug() << "lockStateChanged(): screenIsLocked" << screenIsLocked;

    if (!mobileSpeedDependsOnLockState || !mobileDataActive || hotspotActive) {
        return;
    }

    if (screenIsLocked) {
        qDebug() << "lockStateChanged(): NOW SWITCHING TO" << defaultTech;
        auto& cmd = defaultTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd, this] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
        });
    } else {
        qDebug() << "lockStateChanged(): switching to pref tech";
        qDebug() << "lockStateChanged(): NOW SWITCHING TO" << preferredTech;
        auto& cmd = prefTechPrefCommand;
        QTimer::singleShot(1000, this, [cmd, this] () {
            QProcess process;
            process.start(cmd);
            process.waitForFinished(1000);
        });
    }
}

void BatterySaver::deactivateInterface(const QString& interfaceName)
{

    qDebug() << "deactivateInterface() called for: " << interfaceName;
    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_INDICATOR_NETWORK_SERVICE, DBUS_INDICATOR_NETWORK_PATH, DBUS_INDICATOR_NETWORK_INTERFACE, QStringLiteral("Activate"));
    msg << interfaceName << QVariantList() << QVariantMap();

    QDBusPendingReply<void> reply = QDBusConnection::sessionBus().call(msg);

    if (!reply.isValid())
    {
        qWarning() << "deactivateInterface(): Error activating interface" << interfaceName << reply.error().message();
    } else {
        qDebug() << "deactivateInterface(): Interface Off";
    }
}

bool BatterySaver::interfaceEnabled(const QString& interfaceName)
{
    bool interfaceOn = false;

    QDBusMessage msg = QDBusMessage::createMethodCall(DBUS_INDICATOR_NETWORK_SERVICE, DBUS_INDICATOR_NETWORK_PATH, DBUS_INDICATOR_NETWORK_INTERFACE, QStringLiteral("Describe"));
    msg << interfaceName;
    QDBusMessage reply = QDBusConnection::sessionBus().call(msg);
    if (reply.type() == QDBusMessage::ErrorMessage)
    {
        qWarning() << "interfaceEnabled(): Error while checking interface state" << reply.errorMessage();
    } else {

        bool enabled;
        QDBusSignature signature;
        QVariantList values;

        const QDBusArgument &arg = reply.arguments().at(0).value<QDBusArgument>();
        if (arg.currentType() == QDBusArgument::StructureType)
        {
            arg.beginStructure();
            arg >> enabled;
            arg >> signature;
            arg >> values;
            arg.endStructure();

            interfaceOn = values.at(0).toBool();
            //qDebug() << "enabled? " << interfaceOn;
        } else {
            qDebug() << "interfaceEnabled(): no idea what to do with " << arg.currentType();
        }

    }
    qDebug() << "interfaceEnabled():" << interfaceName << "enabled ?" << interfaceOn;
    return interfaceOn;
}


bool BatterySaver::isScreenLocked()
{
    QDBusInterface greeterIface( "com.lomiri.LomiriGreeter", "/com/lomiri/LomiriGreeter",
                          "com.lomiri.LomiriGreeter" );
    //QDBusReply<bool> reply = greeterIface.call("com.lomiri.LomiriGreeter", "IsActive");
    QVariant reply = greeterIface.property("IsActive");

    if (reply.isValid()) {
        return reply.toBool();
    } else {
        qDebug() << "isScreenLocked(): error: check for lomiri greeter lock state gave invalid response";
    }

    return false;
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    if (!QDBusConnection::sessionBus().isConnected()) {
        qWarning("main(): Cannot connect to the D-Bus session bus.\n"
                 "Please check your system settings and try again.\n");
        return 1;
    }

    if (argc < 7) {
        qWarning("main(): Not enough arguments.\n");
        return 1;
    }

    qDebug() << "main(): called with" << argc << "args, argv:" << argv[1]<< argv[2]<< argv[3]<< argv[4] << argv[5] << argv[6];
    BatterySaver bs(argv[1], argv[2], argv[3], QString(argv[4]) == "true" ? true : false, QString(argv[5]) == "true" ? true : false, QString(argv[6]) == "true" ? true : false);

    return app.exec();
}
