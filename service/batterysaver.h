#ifndef BATTERYSAVER_H
#define BATTERYSAVER_H

#include <QObject>

class BatterySaver: public QObject
{
    Q_OBJECT
public:
    BatterySaver(const QString &simSlot, const QString &defaultNetwork, const QString &prefferedNetwork, bool _disableWifi, bool _disableCellularDataWifiOn, bool _changeNetworkWithLockState, QObject *parent = 0);

private Q_SLOTS:
    void propertiesChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);

    void onNetworkingStatusChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);

    void lockStateChanged(const QString &interface,
                           const QVariantMap &changed,
                           const QStringList &invalidated);

    bool isScreenLocked();

private:
    void deactivateInterface(const QString& interfaceName);
    bool interfaceEnabled(const QString& interfaceName);

    QString defaultTechPrefCommand;
    QString prefTechPrefCommand;
    bool disableWifi;
    bool disableCellularDataWifiOn;
    bool mobileSpeedDependsOnLockState;

    bool screenIsLocked;
    bool mobileDataActive;
    bool hotspotActive;

    QString preferredTech;
    QString defaultTech;

};

#endif // BATTERYSAVER_H
